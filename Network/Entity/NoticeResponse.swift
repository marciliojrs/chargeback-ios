import Domain

public struct NoticeAction: Domain.NoticeAction, AutoCodable {
    public let action: NoticeActionType
    public let title: String
}

public struct NoticeResponse: Domain.NoticeResponse, AutoCodable {
    public let description: String
    //sourcery: useType="NoticeAction"
    //sourcery: castToType="NoticeAction"
    public let primaryAction: Domain.NoticeAction
    //sourcery: useType="NoticeAction"
    //sourcery: castToType="NoticeAction"
    public let secondaryAction: Domain.NoticeAction
    public let title: String
    //sourcery: skipEquality
    //sourcery: useType="HypermediaLinks"
    //sourcery: castToType="HypermediaLinks"
    //sourcery: transformer="ApiLinksTransformer"
    public let links: Domain.HypermediaLinks
}
