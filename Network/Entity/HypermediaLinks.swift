import Domain

public struct HypermediaLinks: Domain.HypermediaLinks {
    public let actions: [HypermediaAction]
}
