import Domain

public struct ChargebackReason: Domain.ChargebackReason, AutoCodable {
    public let id: String
    public let title: String
}

public struct ChargebackReponse: Domain.ChargebackResponse, AutoCodable {
    public let id: String
    public let autoblock: Bool
    public let commentHint: String
    public let title: String
    //sourcery: useType="[ChargebackReason]"
    //sourcery: castToType="[ChargebackReason]"
    public let reasonDetails: [Domain.ChargebackReason]
    //sourcery: skipEquality
    //sourcery: useType="HypermediaLinks"
    //sourcery: castToType="HypermediaLinks"
    //sourcery: transformer="ApiLinksTransformer"
    public let links: Domain.HypermediaLinks
}
