import Domain

public class UseCaseProvider: Domain.UseCaseProvider {
    private let api: API
    public init(api: API) {
        self.api = api
    }

    public func makeNoticeUseCase() -> Domain.NoticeUseCase {
        return NoticeUseCase(api: api)
    }

    public func makeChargebackUseCase() -> Domain.ChargebackUseCase {
        return ChargebackUseCase(api: api)
    }
}
