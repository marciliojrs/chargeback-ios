import Domain
import Foundation
import RxSwift

public struct ChargebackUseCase: Domain.ChargebackUseCase {
    private let api: API

    init(api: API) {
        self.api = api
    }

    public func loadScreen() -> Single<Domain.ChargebackResponse> {
        return api.get(path: "chargeback")
            .map { (data) -> ChargebackReponse in
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase

                return try jsonDecoder.decode(ChargebackReponse.self, from: data)
            }
    }

    public func blockCard(withUrl url: URL) -> Single<Void> {
        return api.post(url: url, body: [:])
            .map { _ in () }
    }

    public func unblockCard(withUrl url: URL) -> Single<Void> {
        return api.post(url: url, body: [:])
            .map { _ in () }
    }

    public func contest(withUrl url: URL, body: [String: Any]) -> Single<Void> {
        return api.post(url: url, body: body)
            .map { _ in () }
    }
}
