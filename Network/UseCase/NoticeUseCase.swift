import Domain
import Foundation
import RxSwift

public struct NoticeUseCase: Domain.NoticeUseCase {
    private let api: API

    init(api: API) {
        self.api = api
    }

    public func loadScreen() -> Single<Domain.NoticeResponse> {
        return api.get(path: "notice")
            .map { (data) -> NoticeResponse in
                let jsonDecoder = JSONDecoder()
                jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase

                return try jsonDecoder.decode(NoticeResponse.self, from: data)
            }
    }
}
