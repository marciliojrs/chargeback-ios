import Foundation
import Domain

final class ApiLinksTransformer: CodingContainerTransformer {
    typealias Input = [String: [String: String]]
    typealias Output = HypermediaLinks

    public func transform(_ decoded: Input) throws -> Output {
        var actions: [HypermediaAction] = []

        decoded.forEach { key, value in
            guard let href = value["href"] else { return }

            if let chargebackUrl = URL(string: href), key == "chargeback" {
                actions.append(HypermediaAction.chargeback(url: chargebackUrl))
            }
            if let blockCardUrl = URL(string: href), key == "blockCard" {
                actions.append(HypermediaAction.blockCard(url: blockCardUrl))
            }
            if let unblockCardUrl = URL(string: href), key == "unblockCard" {
                actions.append(HypermediaAction.unblockCard(url: unblockCardUrl))
            }
            if let selfUrl = URL(string: href), key == "self" {
                actions.append(HypermediaAction.`self`(url: selfUrl))
            }
        }

        return HypermediaLinks(actions: actions)
    }

    public func transform(_ encoded: Output) throws -> Input {
        return Input()
    }
}
