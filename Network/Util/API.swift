import RxSwift
import Foundation

public protocol API: AutoMockable {
    func get(path: String) -> Single<Data>
    func post(path: String) -> Single<Data>
    func post(url: URL, body: [String: Any]) -> Single<Data>
}
