import Foundation
import RxSwift
import RxCocoa

public struct APIClient: API {
    private let baseUrl: URL
    private let session: URLSession

    public init(baseUrl: String, session: URLSession = URLSession.shared) {
        self.baseUrl = URL(string: baseUrl)!
        self.session = session
    }

    public func get(path: String) -> Single<Data> {
        let url = baseUrl.appendingPathComponent(path)
        var request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 15)
        request.httpMethod = "GET"

        return session.rx.data(request: request).asSingle()
    }

    public func post(path: String) -> PrimitiveSequence<SingleTrait, Data> {
        let url = baseUrl.appendingPathComponent(path)
        return post(url: url, body: [:])
    }

    public func post(url: URL, body: [String: Any]) -> Single<Data> {
        var request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 15)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: body)

        return session.rx.data(request: request).asSingle()
    }
}
