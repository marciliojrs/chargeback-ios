import Fakery
import Foundation
import Domain

extension Faker {
    var nubank: NubankFaker { return NubankFaker() }
}

let faker = Faker()

class NubankFaker {
    func notice() -> Domain.NoticeResponse {
        return FakeNoticeResponse()
    }

    func reasons() -> Domain.ChargebackResponse {
        return FakeChargebackResponse()
    }
}

