import Foundation
import Domain

struct FakeNoticeAction: Domain.NoticeAction {
    let action: Domain.NoticeActionType
    let title: String

    init() {
        title = faker.name.name()
        action = faker.number.randomBool() ?
            NoticeActionType.continue :
            NoticeActionType.cancel
    }
}

struct FakeHypermediaLinks: Domain.HypermediaLinks {
    let actions: [HypermediaAction]

    init() {
        do {
            let json: [String: Any] = ["self": faker.internet.url()]
            let action = try JSONDecoder().decode(HypermediaAction.self,
                                                  from: try JSONSerialization.data(withJSONObject: json,
                                                                                   options: .prettyPrinted))
            actions = [action]
        } catch {
            actions = []
        }
    }
}

struct FakeNoticeResponse: Domain.NoticeResponse {
    let description: String
    let primaryAction: Domain.NoticeAction
    let secondaryAction: Domain.NoticeAction
    let title: String
    let links: Domain.HypermediaLinks

    init() {
        description = faker.lorem.sentence()
        primaryAction = FakeNoticeAction()
        secondaryAction = FakeNoticeAction()
        title = faker.name.title()
        links = FakeHypermediaLinks()
    }
}

struct FakeChargebakReason: Domain.ChargebackReason {
    let id: String
    let title: String

    init() {
        id = "\(faker.number.randomInt())"
        title = faker.lorem.sentence()
    }
}

struct FakeChargebackResponse: Domain.ChargebackResponse {
    let id: String
    let autoblock: Bool
    let commentHint: String
    let title: String
    let reasonDetails: [ChargebackReason]
    let links: Domain.HypermediaLinks

    init() {
        id = "\(faker.number.randomInt())"
        autoblock = faker.number.randomBool()
        commentHint = faker.lorem.paragraph()
        title = faker.lorem.sentence()
        reasonDetails = [FakeChargebakReason(), FakeChargebakReason()]
        links = FakeHypermediaLinks()
    }
}
