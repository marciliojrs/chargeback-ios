import Quick
import Nimble
import RxTest
import RxSwift
@testable import Chargeback
@testable import Domain

class ChargebackReasonsViewModelSpec: QuickSpec {
    override func spec() {
        let scheduler = TestScheduler(initialClock: 0)
        let window = UIWindow()
        let coordinator = ChargebackFlowCoordinatorTypeMock(window: window)
        let useCase = ChargebackUseCaseMock()
        var sut: ChargebackReasonsViewModel!

        describe("ChargebackReasonsViewModel") {
            context("output") {
                context("with valid screen response") {
                    var fakeReasons: ChargebackResponse!
                    beforeEach {
                        fakeReasons = faker.nubank.reasons()
                        useCase.loadScreenReturnValue = scheduler.createHotObservable([
                            .next(210, fakeReasons),
                            .completed(210)
                        ]).asSingle()
                    }

                    it("should return a viewState for a valid screen response") {
                        sut = ChargebackReasonsViewModel(coordinator: coordinator, useCase: useCase)
                        let res = scheduler.start { sut.output.viewState.asObservable() }
                        do {
                            let expected = ChargebackReasonsViewState(title: fakeReasons.title.uppercased(),
                                              isBlocked: fakeReasons.autoblock,
                                              commentHint: try fakeReasons.commentHint.html2Attributed(),
                                              reasons: fakeReasons.reasonDetails)

                            let event = res.events.first
                            expect(event?.value.element).to(equal(expected))
                        } catch {
                            fail("error")
                        }
                    }

                    it("should correctly stream the loading state of screen") {
                        sut = ChargebackReasonsViewModel(coordinator: coordinator, useCase: useCase)
                        let res = scheduler.start { sut.output.loading }
                        let event = res.events.first
                        expect(event?.value.element).to(beFalse())
                    }

                    it("should call cordinator's back() when cancel") {
                        sut = ChargebackReasonsViewModel(coordinator: coordinator, useCase: useCase)
                        sut.input.cancel.on(.next(()))
                        expect(coordinator.backAnimatedCalled).to(beTrue())
                    }
                }
            }
        }
    }
}
