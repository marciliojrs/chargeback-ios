import Quick
import Nimble
import RxTest
@testable import Chargeback

class ChargebackFinishViewModelSpec: QuickSpec {
    override func spec() {
        let window = UIWindow()
        let coordinator = ChargebackFlowCoordinatorTypeMock(window: window)
        var sut: ChargebackFinishViewModel!

        describe("ChargebackFinishViewModel") {
            context("input") {
                it("should finish cordinator when tap close") {
                    sut = ChargebackFinishViewModel(coordinator: coordinator)
                    sut.input.close.on(.next(()))
                    expect(coordinator.finishWithTransitionCalled).to(beTrue())
                }
            }
        }
    }
}
