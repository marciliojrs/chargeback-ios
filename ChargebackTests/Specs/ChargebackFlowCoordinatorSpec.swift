import Quick
import Nimble
import RxTest
import RxSwift
@testable import Chargeback
@testable import Domain

class ChargebackFlowCoordinatorSpec: QuickSpec {
    override func spec() {
        let window = UIWindow()
        let coordinator = ChargebackFlowCoordinator(window: window)
        let provider = UseCaseProviderMock()

        let chargebackUseCaseMock = ChargebackUseCaseMock()
        chargebackUseCaseMock.loadScreenReturnValue = Single.just(faker.nubank.reasons())
        provider.makeChargebackUseCaseReturnValue = chargebackUseCaseMock

        let noticeUseCaseMock = NoticeUseCaseMock()
        noticeUseCaseMock.loadScreenReturnValue = Single.just(faker.nubank.notice())
        provider.makeNoticeUseCaseReturnValue = noticeUseCaseMock

        describe("ChargebackFlowCoordinator") {
            beforeEach {
                coordinator.start(withProvider: provider)
            }

            it("make notice use case when start coordinator and present ChargebackNoticeViewController") {
                expect(provider.makeNoticeUseCaseCalled).to(beTrue())
                let topViewController = coordinator.currentViewController?.navigationController?.topViewController
                expect(topViewController is ChargebackNoticeViewController).to(beTrue())
            }

            it("make chargeback use case when push to route chargebackReasons and present ChargebackReasonsViewController") {
                coordinator.route(to: ChargebackFlowCoordinator.Path.chargebackReasons)
                expect(provider.makeChargebackUseCaseCalled).to(beTrue())

                let topViewController = coordinator.currentViewController?.navigationController?.topViewController
                expect(topViewController is ChargebackReasonsViewController).to(beTrue())
            }

            it("present ChargebackFinishViewController when route to finish") {
                coordinator.route(to: ChargebackFlowCoordinator.Path.finish)

                let topViewController = coordinator.currentViewController?.navigationController?.topViewController
                expect(topViewController is ChargebackFinishViewController).to(beTrue())
            }
        }
    }
}
