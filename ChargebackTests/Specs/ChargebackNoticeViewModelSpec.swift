import Quick
import Nimble
import RxTest
@testable import Chargeback
@testable import Domain

class ChargebackNoticeViewModelSpec: QuickSpec {
    override func spec() {
        let scheduler = TestScheduler(initialClock: 0)
        let window = UIWindow()
        let coordinator = ChargebackFlowCoordinatorTypeMock(window: window)
        let useCase = NoticeUseCaseMock()
        var sut: ChargebackNoticeViewModel!

        describe("ChargebackNoticeViewModel") {
            context("output") {
                context("with valid screen response") {
                    var fakeNotice: NoticeResponse!
                    beforeEach {
                        fakeNotice = faker.nubank.notice()
                        useCase.loadScreenReturnValue = scheduler.createHotObservable([
                            .next(210, fakeNotice),
                            .completed(220)
                        ]).asSingle()

                        sut = ChargebackNoticeViewModel(coordinator: coordinator, useCase: useCase)
                    }

                    it("should return a viewState for a valid screen response") {
                        let res = scheduler.start { sut.output.viewState.asObservable() }
                        do {
                            let expected = ChargebackNoticeViewState(title: fakeNotice.title,
                                             description: try fakeNotice.description.html2Attributed(),
                                             continueButtonTitle: fakeNotice.primaryAction.title.uppercased(),
                                             closeButtonTitle: fakeNotice.secondaryAction.title.uppercased())
                            let event = res.events.first
                            expect(event?.value.element).to(equalDiff(expected))
                        } catch {
                            fail("error")
                        }
                    }

                    it("should correctly identify loading states") {
                        let res = scheduler.start { sut.output.loading }
                        res.events.enumerated().forEach({ (index, event) in
                            expect(event.value.element).to(equal(index == 0 ? true : false))
                        })
                    }
                }
            }

            context("input") {
                beforeEach {
                    useCase.loadScreenReturnValue = scheduler.createHotObservable([
                        .next(210, faker.nubank.notice()),
                        .completed(220)
                    ]).asSingle()

                    sut = ChargebackNoticeViewModel(coordinator: coordinator, useCase: useCase)
                }

                it("should trigger the coordinator when tap continue") {
                    sut.input.continue.onNext(())
                    expect({
                        guard case ChargebackFlowCoordinator.Path.chargebackReasons = coordinator.routeToReceivedPath! else {
                            return .failed(reason: "wrong enum case")
                        }

                        return .succeeded
                    }).to(succeed())
                }
            }
        }
    }
}
