# Nubank Recruitment Test

## Getting Started
These instructions will guide you to clone and setup the project on your local machine.

### Prerequisites
* Xcode 9.3
* Homebrew
* Bundler

### Installing
After clone the repository, please run the following script in your preferred terminal application.

`./bin/setup`

### Running 
To run the application just open `Chargeback.xcworkspace` and play.
