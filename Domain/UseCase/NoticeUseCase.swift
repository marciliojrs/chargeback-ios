import RxSwift

public protocol NoticeUseCase: AutoMockable {
    func loadScreen() -> Single<NoticeResponse>
}
