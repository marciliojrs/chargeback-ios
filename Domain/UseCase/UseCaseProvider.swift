import Foundation

public protocol UseCaseProvider: class, AutoMockable {
    func makeNoticeUseCase() -> NoticeUseCase
    func makeChargebackUseCase() -> ChargebackUseCase
}
