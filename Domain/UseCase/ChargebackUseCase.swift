import RxSwift

public protocol ChargebackUseCase: AutoMockable {
    func loadScreen() -> Single<ChargebackResponse>
    func blockCard(withUrl url: URL) -> Single<Void>
    func unblockCard(withUrl url: URL) -> Single<Void>
    func contest(withUrl url: URL, body: [String: Any]) -> Single<Void>
}
