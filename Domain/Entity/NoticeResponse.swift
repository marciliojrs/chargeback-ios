import Foundation

public enum NoticeActionType: String, AutoEquatable, Codable {
    case `continue`
    case cancel
}

public protocol NoticeAction: AutoEquatable {
    var action: NoticeActionType { get }
    var title: String { get }
}

public protocol NoticeResponse: AutoEquatable {
    var description: String { get }
    var primaryAction: NoticeAction { get }
    var secondaryAction: NoticeAction { get }
    var title: String { get }
    //sourcery: skipEquality
    var links: HypermediaLinks { get }
}
