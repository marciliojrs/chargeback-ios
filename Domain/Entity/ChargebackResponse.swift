import Foundation

public protocol ChargebackReason: AutoEquatable {
    var id: String { get }
    var title: String { get }
}

public protocol ChargebackResponse: AutoEquatable {
    var id: String { get }
    var autoblock: Bool { get }
    var commentHint: String { get }
    var title: String { get }
    //sourcery: arrayEquality
    var reasonDetails: [ChargebackReason] { get }
    //sourcery: skipEquality
    var links: HypermediaLinks { get }
}
