import Foundation

public enum HypermediaAction: Codable {
    enum CodingError: Error { case decoding(String) }
    enum CodableKeys: String, CodingKey {
        case chargeback
        case blockCard
        case unblockCard
        case `self`
    }

    case chargeback(url: URL)
    case `self`(url: URL)
    case blockCard(url: URL)
    case unblockCard(url: URL)
    case unknown

    public var url: URL? {
        switch self {
        case .blockCard(let url), .`self`(let url), .unblockCard(let url), .chargeback(let url): return url
        default: return nil
        }
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodableKeys.self)

        if let url = try? values.decode(URL.self, forKey: .chargeback) {
            self = .chargeback(url: url)
            return
        } else if let url = try? values.decode(URL.self, forKey: .`self`) {
            self = .`self`(url: url)
            return
        } else if let url = try? values.decode(URL.self, forKey: .blockCard) {
            self = .blockCard(url: url)
            return
        } else if let url = try? values.decode(URL.self, forKey: .unblockCard) {
            self = .unblockCard(url: url)
            return
        }

        self = .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodableKeys.self)

        switch self {
        case .chargeback(let url):
            try container.encode(url, forKey: .chargeback)
        case .blockCard(let url):
            try container.encode(url, forKey: .blockCard)
        case .unblockCard(let url):
            try container.encode(url, forKey: .unblockCard)
        case .`self`(let url):
            try container.encode(url, forKey: .`self`)
        default: break
        }
    }
}

extension HypermediaAction: Equatable {
    public static func == (lhs: HypermediaAction, rhs: HypermediaAction) -> Bool {
        switch (lhs, rhs) {
        case (.blockCard, .blockCard):
            return true
        case (.unblockCard, .unblockCard):
            return true
        case (.`self`, .`self`):
            return true
        case (.chargeback, .chargeback):
            return true
        default: return false
        }
    }
}

public protocol HypermediaLinks {
    var actions: [HypermediaAction] { get }
}
