import Quick
import Nimble
import RxTest
@testable import ChargebackNetwork

class NoticeUseCaseSpec: QuickSpec {
    override func spec() {
        let api = APIMock()
        let useCase = NoticeUseCase(api: api)
        let scheduler = TestScheduler(initialClock: 0)

        describe("NoticeUseCase") {
            context("loadScreen()") {
                it("should return a notice response instance for valid json") {
                    do {
                        let jsonData = try Data(contentsOf: R.file.noticeEndpointValidJson()!)
                        api.getPathReturnValue = scheduler.createHotObservable([
                            .next(210, jsonData),
                            .completed(220)
                            ]).asSingle()

                        let res = scheduler.start { useCase.loadScreen().asObservable() }
                        let event = res.events.first
                        expect(api.getPathReceivedPath).to(equal("notice"))
                        expect(event?.value.element).toNot(beNil())
                        expect(event?.value.error).to(beNil())
                    } catch {
                        fail("error")
                    }
                }

                it("should return an error for a invalid json") {
                    let json = "{title: \"test\"}"
                    api.getPathReturnValue = scheduler.createHotObservable([
                        .next(210, json.data(using: .utf8)!),
                        .completed(220)
                        ]).asSingle()

                    let res = scheduler.start { useCase.loadScreen().asObservable() }
                    let event = res.events.first
                    expect(api.getPathReceivedPath).to(equal("notice"))
                    expect(event?.value.error).toNot(beNil())
                    expect(event?.value.element).to(beNil())
                }
            }
        }
    }
}
