import Quick
import Nimble
import RxTest
import Fakery
@testable import ChargebackNetwork

//swiftlint:disable function_body_length
class ChargebackUseCaseSpec: QuickSpec {
    override func spec() {
        let scheduler = TestScheduler(initialClock: 0)
        let api = APIMock()
        let useCase = ChargebackUseCase(api: api)
        let faker = Faker(locale: "en-US")

        describe("ChargebackUseCaseSpec") {
            context("loadScreen method") {
                it("should return a valid ChargebackResponse for valid json") {
                    do {
                        let jsonData = try Data(contentsOf: R.file.chargebackEndpointValidJson()!)
                        api.getPathReturnValue = scheduler.createHotObservable([
                            .next(210, jsonData),
                            .completed(220)
                        ]).asSingle()

                        let res = scheduler.start { useCase.loadScreen().asObservable() }
                        let event = res.events.first
                        expect(api.getPathReceivedPath).to(equal("chargeback"))
                        expect(event?.value.element).toNot(beNil())
                        expect(event?.value.error).to(beNil())
                    } catch {
                        fail("error")
                    }
                }

                it("should return an error for invalid json") {
                    api.getPathReturnValue = scheduler.createHotObservable([
                        .next(210, "{\"title\": \"test\"}".data(using: .utf8)!),
                        .completed(220)
                    ]).asSingle()

                    let res = scheduler.start { useCase.loadScreen().asObservable() }
                    let event = res.events.first
                    expect(api.getPathReceivedPath).to(equal("chargeback"))
                    expect(event?.value.element).to(beNil())
                    expect(event?.value.error).toNot(beNil())
                }
            }

            context("blockCard method") {
                it("should not return an error when call block card") {
                    let anyUrl = URL(string: faker.internet.url())!
                    api.postUrlBodyReturnValue = scheduler.createHotObservable([
                        .next(210, Data()),
                        .completed(220)
                    ]).asSingle()

                    let res = scheduler.start { useCase.blockCard(withUrl: anyUrl).asObservable() }
                    let event = res.events.first
                    expect(event?.value.error).to(beNil())
                    expect(event?.value.element).toNot(beNil())
                }
            }

            context("unblockCard method") {
                it("should not return an error when call unblock card") {
                    let anyUrl = URL(string: faker.internet.url())!
                    api.postUrlBodyReturnValue = scheduler.createHotObservable([
                        .next(210, Data()),
                        .completed(220)
                        ]).asSingle()

                    let res = scheduler.start { useCase.unblockCard(withUrl: anyUrl).asObservable() }
                    let event = res.events.first
                    expect(event?.value.error).to(beNil())
                    expect(event?.value.element).toNot(beNil())
                }
            }

            context("contest method") {
                it("should not return an error when call contest") {
                    let anyUrl = URL(string: faker.internet.url())!
                    let anyDict: [String: Any] = [:]
                    api.postUrlBodyReturnValue = scheduler.createHotObservable([
                        .next(210, Data()),
                        .completed(220)
                        ]).asSingle()

                    let res = scheduler.start { useCase.contest(withUrl: anyUrl, body: anyDict).asObservable() }
                    let event = res.events.first
                    expect(event?.value.error).to(beNil())
                    expect(event?.value.element).toNot(beNil())
                }
            }
        }
    }
}
//swiftlint:enable function_body_length
