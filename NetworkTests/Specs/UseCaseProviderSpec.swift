import Foundation
import Quick
import Nimble
@testable import ChargebackNetwork

class UseCaseProviderSpec: QuickSpec {
    override func spec() {
        describe("UseCaseProvider") {
            let api = APIMock()
            let factory = UseCaseProvider(api: api)

            it("should return an instance of NoticeUseCase when makeNoticeUseCase() is called") {
                expect(factory.makeNoticeUseCase()).to(beAKindOf(NoticeUseCase.self))
            }

            it("should return an instance of ChargebackUseCase when makeChargebackUseCase() is called") {
                expect(factory.makeChargebackUseCase()).to(beAKindOf(ChargebackUseCase.self))
            }
        }
    }
}
