import Foundation
import RxSwift
import RxCocoa
import Domain

struct ChargebackFinishViewModel: RxViewModel {
    struct Input {
        let close: AnyObserver<Void>
    }
    struct Output { }

    let input: Input
    let output: Output

    private weak var coordinator: ChargebackFlowCoordinatorType?
    private let bag = DisposeBag()

    private let closeSubject = PublishSubject<Void>()

    init(coordinator: ChargebackFlowCoordinatorType) {
        self.coordinator = coordinator

        input = Input(close: closeSubject.asObserver())
        output = Output()

        bind()
    }

    private func bind() {
        bag << closeSubject.subscribe(onNext: {
            self.coordinator?.finish(withTransition: .dismiss)
        })
    }
}
