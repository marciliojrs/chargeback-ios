import Foundation
import UIKit
import RxSwift
import RxCocoa
import Layout

final class ChargebackFinishViewController: BaseViewController<ChargebackFinishViewModel> {
    @objc var closeButton: UIButton!

    override var constants: LayoutConstants {
        var constants = super.constants
        constants["strings.title"] = R.string.chargebackFinish.alertTitle()
        constants["strings.body"] = R.string.chargebackFinish.alertBody()
        constants["strings.button.title"] = R.string.chargebackFinish.buttonClose()

        return constants
    }

    override func rebuildView() {
        bag << closeButton.rx.tap.bind(to: viewModel.input.close)
    }
}
