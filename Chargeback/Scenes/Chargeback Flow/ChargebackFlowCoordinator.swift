import UIKit
import ChargebackNetwork
import RxSwift
import Domain

protocol ChargebackFlowCoordinatorType: CoordinatorType {
    func start(withProvider provider: Domain.UseCaseProvider)
}

protocol ChargebackFlowCoordinatorDelegate: class {
    func chargeBackFlowCoordinatorDidFinish(_ coordintor: ChargebackFlowCoordinator)
}

final class ChargebackFlowCoordinator: ChargebackFlowCoordinatorType {
    enum Path: CoordinatorPath, AutoEquatable {
        case chargebackReasons
        case finish
    }

    var window: UIWindow!
    var children: [CoordinatorType]
    private var useCaseProvider: Domain.UseCaseProvider!

    weak var delegate: ChargebackFlowCoordinatorDelegate?

    init(window: UIWindow) {
        self.window = window
        self.children = []
    }

    func start(withProvider provider: Domain.UseCaseProvider) {
        self.useCaseProvider = provider
        start()
    }

    func start() {
        if useCaseProvider == nil {
            useCaseProvider = UseCaseProvider(api: APIClient(baseUrl: "https://nu-mobile-hiring.herokuapp.com"))
        }

        let viewModel = ChargebackNoticeViewModel(coordinator: self, useCase: useCaseProvider.makeNoticeUseCase())
        let viewController = ChargebackNoticeViewController(layoutName: R.file.chargebackNoticeViewXml.name,
                viewModel: viewModel)

        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.isNavigationBarHidden = true
        window.rootViewController = navigationController
    }

    func route(to path: CoordinatorPath) {
        guard let path = path as? Path else { return }

        switch path {
        case .chargebackReasons:
            let viewModel = ChargebackReasonsViewModel(coordinator: self,
                    useCase: useCaseProvider.makeChargebackUseCase())
            let viewController = ChargebackReasonsViewController(layoutName: R.file.chargebackReasonsViewXml.name,
                    viewModel: viewModel)

            currentViewController?.navigationController?.pushViewController(viewController, animated: false)
        case .finish:
            let viewModel = ChargebackFinishViewModel(coordinator: self)
            let viewController = ChargebackFinishViewController(layoutName: R.file.chargebackFinishViewXml.name,
                                                                viewModel: viewModel)

            currentViewController?.navigationController?.pushViewController(viewController, animated: false)
        }
    }

    func finish(withTransition transition: CloseTransition) {
        currentViewController?.navigationController?.popToRootViewController(animated: false)
    }
}
