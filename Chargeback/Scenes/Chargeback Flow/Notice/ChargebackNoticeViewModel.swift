import Foundation
import RxSwift
import RxCocoa
import Domain

struct ChargebackNoticeViewModel: RxViewModel {
    struct Input {
        let `continue`: AnyObserver<Void>
    }
    struct Output {
        let viewState: Driver<ChargebackNoticeViewState>
        let loading: Observable<Bool>
    }

    let input: Input
    let output: Output

    private weak var coordinator: ChargebackFlowCoordinatorType?
    private let useCase: NoticeUseCase
    private let bag = DisposeBag()

    private let activityTracker = RxActivityTracker()

    private let noticeResponseSubject = ReplaySubject<NoticeResponse>.create(bufferSize: 1)
    private let continueSubject = PublishSubject<Void>()

    init(coordinator: ChargebackFlowCoordinatorType, useCase: NoticeUseCase) {
        self.coordinator = coordinator
        self.useCase = useCase

        let viewState = noticeResponseSubject.map { (noticeResponse: NoticeResponse) -> ChargebackNoticeViewState in
            let span = """
            <span style=\"font-family: '-apple-system';
                    font-size: \(Font.footnote.font.pointSize)\">%@</span>
            """
            let modifiedHTML = NSString(format: span as NSString, noticeResponse.description) as String
            return ChargebackNoticeViewState(title: noticeResponse.title,
                    description: try modifiedHTML.html2Attributed(),
                    continueButtonTitle: noticeResponse.primaryAction.title.uppercased(),
                    closeButtonTitle: noticeResponse.secondaryAction.title.uppercased())
        }.asDriver(onErrorJustReturn: ChargebackNoticeViewState.empty)

        input = Input(continue: continueSubject.asObserver())
        output = Output(viewState: viewState, loading: activityTracker.asObservable())

        bind()
    }

    private func bind() {
        bag << useCase.loadScreen().track(activity: activityTracker).bind(to: noticeResponseSubject)

        bag << continueSubject
            .subscribe(onNext: { _ in
                self.coordinator?.route(to: ChargebackFlowCoordinator.Path.chargebackReasons)
            })
    }
}
