import Foundation
import UIKit
import RxSwift
import RxCocoa

import Layout

final class ChargebackNoticeViewController: BaseViewController<ChargebackNoticeViewModel> {
    @objc var continueButton: UIButton!

    override var initialViewState: ViewState {
        return ["state": ChargebackNoticeViewState.empty, "isLoading": false]
    }

    override func rebuildView() {
        guard let node = layoutNode else { return }

        bag << viewModel.output.viewState
            .map { ["state": $0] }
            .drive(node.rx.setState)

        bag << continueButton.rx.tap.bind(to: viewModel.input.continue)

        bag << viewModel.output.loading.subscribe(onNext: { (isLoading) in
            node.setState(["isLoading": isLoading])
        })
    }
}
