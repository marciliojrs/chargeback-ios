import Foundation

struct ChargebackNoticeViewState: AutoEquatable, AutoLenses {
    static let empty = ChargebackNoticeViewState(title: "", description: NSAttributedString(),
            continueButtonTitle: "", closeButtonTitle: "")

    let title: String
    //sourcery: skipEquality
    let description: NSAttributedString
    let continueButtonTitle: String
    let closeButtonTitle: String
}
