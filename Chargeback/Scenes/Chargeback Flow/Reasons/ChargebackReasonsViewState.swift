import Foundation
import Domain

struct ChargebackReasonsViewState: AutoLenses, AutoEquatable {
    static let empty = ChargebackReasonsViewState(title: "", isBlocked: true, commentHint: NSAttributedString(),
                                                  reasons: [])

    let title: String
    let isBlocked: Bool
    //sourcery: skipEquality
    let commentHint: NSAttributedString
    //sourcery: skipEquality
    let reasons: [ChargebackReason]

    var blockedText: String {
        return isBlocked ? R.string.chargebackReasons.blockedText() : R.string.chargebackReasons.unblockedText()
    }
    var blockedImageName: String {
        return isBlocked ? R.image.ic_chargeback_lock.name : R.image.ic_chargeback_unlock.name
    }

    func mapToViewState() -> ViewState {
        return ["state": self,
                "state.blockedText": self.blockedText,
                "state.blockedImageName": self.blockedImageName]
    }
}
