import Foundation
import UIKit
import RxSwift
import RxCocoa
import Layout

final class ChargebackReasonsViewController: BaseViewController<ChargebackReasonsViewModel> {
    @objc var cancelButton: UIButton!
    @objc var toggleBlockButton: UIButton!
    @objc var contestButton: UIButton!
    @objc var textView: KMPlaceholderTextView!

    override var initialViewState: ViewState {
        var state = ChargebackReasonsViewState.empty.mapToViewState()
        state["isLoading"] = false

        return state
    }

    override func rebuildView() {
        guard let node = layoutNode else { return }

        bag << viewModel.output.viewState
            .do(onNext: { (state) in
                let parent = node.childNode(withID: "reasonsView")
                parent?.children.forEach { $0.removeFromParent() }
                state.reasons.enumerated().forEach { (idx, reason) in
                    let labelNode = LayoutNode(view: UILabel.self,
                                               expressions: ["left": "8",
                                                             "top": "previous.top + \(idx * 50)",
                                                             "text": reason.title,
                                                             "font": "{theme.font.footnote}",
                                                             "textColor": "{#\(reason.id).isOn ? #417505 : #222222}"])
                    let switchNode = LayoutNode(view: UISwitch.self,
                                                id: "\(reason.id)",
                                                expressions: ["right": "100% - 8",
                                                              "center.y": "previous.center.y",
                                                              "isOn": "false"])

                    parent?.addChild(labelNode)
                    parent?.addChild(switchNode)
                }
            }).map { $0.mapToViewState() }
            .drive(node.rx.setState)

        bag << viewModel.output.loading.subscribe(onNext: { (isLoading) in
            node.setState(["isLoading": isLoading])
        })

        let toggleTap = toggleBlockButton.rx.tap.withLatestFrom(viewModel.output.viewState)
            .share(replay: 1, scope: .whileConnected)

        bag << contestButton.rx.tap.withLatestFrom(viewModel.output.viewState)
            .map { [textView] (state) in
                let reasonsJsonArray: [[String: Any]] = state.reasons.map { (reason) -> [String: Any] in
                    let switchNode = node.childNode(withID: reason.id)
                    let switchControl = switchNode?.view as? UISwitch
                    return ["id": reason.id, "response": String(describing: switchControl?.isOn ?? false)]
                }

                return ["comment": textView?.text ?? "", "reason_details": reasonsJsonArray]
            }.bind(to: viewModel.input.contest)

        bag << [
            cancelButton.rx.tap.bind(to: viewModel.input.cancel),
            textView.rx.text.map({ !$0.isNilOrEmpty }).bind(to: contestButton.rx.isEnabled),
            toggleTap.filter({ $0.isBlocked }).mapToVoid().bind(to: viewModel.input.block),
            toggleTap.filter({ !$0.isBlocked }).mapToVoid().bind(to: viewModel.input.unblock)
        ]
    }
}
