import Foundation
import RxSwift
import RxCocoa
import Domain

struct ChargebackReasonsViewModel: RxViewModel {
    struct Input {
        let contest: AnyObserver<[String: Any]>
        let cancel: AnyObserver<Void>
        let block: AnyObserver<Void>
        let unblock: AnyObserver<Void>
        let reason: AnyObserver<String>
    }
    struct Output {
        let viewState: Driver<ChargebackReasonsViewState>
        let loading: Observable<Bool>
    }

    let input: Input
    let output: Output

    private weak var coordinator: ChargebackFlowCoordinatorType?
    private let useCase: ChargebackUseCase
    private let bag = DisposeBag()

    private let loadScreenActivity = RxActivityTracker()
    private let blockActivity = RxActivityTracker()
    private let unblockActivity = RxActivityTracker()
    private let contestActivity = RxActivityTracker()

    private let blockSubject = PublishSubject<Void>()
    private let unblockSubject = PublishSubject<Void>()
    private let contestSubject = PublishSubject<[String: Any]>()
    private let cancelSubject = PublishSubject<Void>()
    private let reasonSubject = PublishSubject<String>()
    private let loadingSubject = PublishSubject<Bool>()
    private let viewState = PublishSubject<ChargebackReasonsViewState>()
    private let responseSubject = PublishSubject<ChargebackResponse>()
    private let postRelay = BehaviorRelay<URL?>(value: nil)

    init(coordinator: ChargebackFlowCoordinatorType, useCase: ChargebackUseCase) {
        self.coordinator = coordinator
        self.useCase = useCase

        input = Input(contest: contestSubject.asObserver(), cancel: cancelSubject.asObserver(),
                      block: blockSubject.asObserver(), unblock: unblockSubject.asObserver(),
                      reason: reasonSubject.asObserver())
        output = Output(viewState: viewState.asDriver(onErrorJustReturn: .empty),
                        loading: loadingSubject)

        bind()
    }

    private func bind() {
        handleViewStateMapper()
        handleUnblockCard()
        handleBlockCard()

        bag << Observable.combineLatest(loadScreenActivity.asObservable().startWith(false),
                                        blockActivity.asObservable().startWith(false),
                                        unblockActivity.asObservable().startWith(false),
                                        contestActivity.asObservable().startWith(false)) {
                                            return $0 || $1 || $2 || $3
            }.distinctUntilChanged()
            .bind(to: loadingSubject)

        bag << useCase.loadScreen().asObservable()
            .subscribe(onNext: { self.responseSubject.onNext($0) })

        bag << cancelSubject.subscribe(onNext: {
            self.coordinator?.back(animated: false)
        })

        bag << contestSubject
            .flatMapLatest { (json) -> Observable<Void> in
                guard let url = self.postRelay.value else { return Observable.error(RuntimeError.contestUrlNotFound) }
                return self.useCase.contest(withUrl: url, body: json).track(activity: self.contestActivity)
            }.observeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in self.coordinator?.route(to: ChargebackFlowCoordinator.Path.finish) })
    }

    private func handleViewStateMapper() {
        bag << responseSubject
            .flatMap { (response) -> Observable<ChargebackResponse> in
                if response.autoblock {
                    return self.blockCard(using: response.links)
                        .map { _ in response }
                } else {
                    return Observable.just(response)
                }
            }.map { (response) -> ChargebackReasonsViewState in
                self.setChargebackUrl(using: response.links)

                let span = """
                <span style=\"font-family: '-apple-system';
                font-size: \(Font.footnote.font.pointSize)\">%@</span>
                """
                let modifiedHTML = NSString(format: span as NSString, response.commentHint) as String

                return ChargebackReasonsViewState(title: response.title.uppercased(),
                                                  isBlocked: response.autoblock,
                                                  commentHint: try modifiedHTML.html2Attributed(),
                                                  reasons: response.reasonDetails)
            }.track(activity: loadScreenActivity)
            .observeOn(MainScheduler.instance)
            .bind(to: viewState)
    }

    private func handleUnblockCard() {
        bag << unblockSubject.withLatestFrom(responseSubject)
            .flatMapLatest { (chargeback: ChargebackResponse) -> Observable<Void> in
                let action: HypermediaAction? = chargeback.links.actions.first(where: { (action) in
                    if case .unblockCard = action {
                        return true
                    } else {
                        return false
                    }
                })

                if let url = action?.url {
                    return self.useCase.unblockCard(withUrl: url).track(activity: self.unblockActivity)
                } else {
                    return Observable.error(RuntimeError.networkError)
                }
            }.withLatestFrom(viewState)
            .subscribe(onNext: { (state) in
                self.viewState.onNext(ChargebackReasonsViewState.isBlockedLens.set(true, state))
            })
    }

    private func handleBlockCard() {
        bag << blockSubject.withLatestFrom(responseSubject)
            .flatMapLatest { (chargeback: ChargebackResponse) -> Observable<Void> in
                return self.blockCard(using: chargeback.links)
            }.withLatestFrom(viewState)
            .subscribe(onNext: { (state) in
                self.viewState.onNext(ChargebackReasonsViewState.isBlockedLens.set(false, state))
            })
    }

    private func blockCard(using links: HypermediaLinks) -> Observable<Void> {
        let action: HypermediaAction? = links.actions.first(where: { (action) in
            if case .blockCard = action {
                return true
            } else {
                return false
            }
        })

        if let url = action?.url {
            return self.useCase.blockCard(withUrl: url).track(activity: blockActivity)
        } else {
            return Observable.error(RuntimeError.networkError)
        }
    }

    private func setChargebackUrl(using links: HypermediaLinks) {
        let action: HypermediaAction? = links.actions.first(where: { (action) in
            if case .`self` = action {
                return true
            } else {
                return false
            }
        })

        postRelay.accept(action?.url)
    }
}
