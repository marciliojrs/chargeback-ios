import UIKit

protocol AppCoordinatorType: CoordinatorType { }

final class AppCoordinator: AppCoordinatorType {
    private(set) var window: UIWindow!
    var children: [CoordinatorType]

    required init(window: UIWindow) {
        self.window = window
        self.children = []
    }

    func start() {
        let coordinator = ChargebackFlowCoordinator(window: window)
        coordinator.delegate = self
        setupAndInit(coordinator: coordinator)
    }

    func finish(withTransition transition: CloseTransition) {}

    func route(to path: CoordinatorPath) {}
}

extension AppCoordinator: ChargebackFlowCoordinatorDelegate {
    func chargeBackFlowCoordinatorDidFinish(_ coordinator: ChargebackFlowCoordinator) {
        remove(child: coordinator)
    }
}
