enum RuntimeError: Error {
    case networkError
    case contestUrlNotFound
}
