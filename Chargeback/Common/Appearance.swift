import UIKit

enum Font {
    case body, bodyBold, footnote, footnoteBold, title

    var font: UIFont {
        switch self {
        case .title:
            return UIFont.preferredFont(forTextStyle: .title1)
        case .body:
            return UIFont.preferredFont(forTextStyle: .body)
        case .bodyBold:
            return UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .body).pointSize)
        case .footnote:
            return UIFont.preferredFont(forTextStyle: .footnote)
        case .footnoteBold:
            return UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .footnote).pointSize)
        }
    }

    static var layoutConstants: [String: Any] {
        return ["theme.font.body": Font.body.font,
                "theme.font.bodyBold": Font.bodyBold.font,
                "theme.font.footnote": Font.footnote.font,
                "theme.font.title": Font.title.font,
                "theme.font.footnoteBold": Font.footnoteBold.font]
    }
}

extension UIColor {
    static let purple: UIColor = UIColor(rgb: 0x6e2b77)
    static let black: UIColor = UIColor(rgb: 0x222222)
    static let hint: UIColor = UIColor(rgb: 0x999999)
    static let background: UIColor = UIColor(rgb: 0xfdfdfd)
    static let green: UIColor = UIColor(rgb: 0x417505)
    static let red: UIColor = UIColor(rgb: 0xd5171b)
    static let closeGray: UIColor = UIColor(rgb: 0x808191)
    static let disabledGray: UIColor = UIColor(rgb: 0xcccccc)
    static let enabledPurple: UIColor = UIColor(rgb: 0x6e2b77)
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
