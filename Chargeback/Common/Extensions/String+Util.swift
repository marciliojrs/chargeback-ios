import Foundation

extension Optional where Wrapped == String {
    var isNilOrEmpty: Bool {
        return self == nil || (self?.isEmpty == true)
    }
}
