import RxSwift

extension Observable {
    func mapToVoid() -> Observable<Void> {
        return self.map { _ in () }
    }
}
