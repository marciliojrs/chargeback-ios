import RxSwift
import RxCocoa
import Layout

extension Reactive where Base: LayoutNode {
    var setState: Binder<Any> {
        return Binder(self.base) { (node, state) in
            node.setState(state)
        }
    }
}
