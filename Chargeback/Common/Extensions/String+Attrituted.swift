import Foundation

extension String {
    var attributed: NSAttributedString {
        return NSAttributedString(string: self)
    }

    func html2Attributed() throws -> NSAttributedString {
        return try NSAttributedString(data: data(using: String.Encoding.utf8)!,
                                      options: [.documentType: NSAttributedString.DocumentType.html,
                                                .characterEncoding: String.Encoding.utf8.rawValue],
                                      documentAttributes: nil)
    }
}
