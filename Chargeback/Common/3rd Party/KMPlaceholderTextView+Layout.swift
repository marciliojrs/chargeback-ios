import Layout

extension KMPlaceholderTextView {
    open override class var expressionTypes: [String: RuntimeType] {
        var types = super.expressionTypes
        types["placeholderLabel.attributedText"] = .nsAttributedString
        return types
    }

    open override func setValue(_ value: Any, forExpression name: String) throws {
        switch name {
        case "placeholderLabel.attributedText":
            placeholderLabel.attributedText = value as? NSAttributedString
        default:
            try super.setValue(value, forExpression: name)
        }
    }
}
